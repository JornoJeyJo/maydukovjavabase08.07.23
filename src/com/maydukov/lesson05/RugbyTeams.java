import java.util.Random;

public class RugbyTeams {
    public static void main(String[] args) {
        int[] teamAgesChicks = generateTeamAges();
        int[] teamAgesBulls = generateTeamAges();

        System.out.println("Команда чайок:");
        printTeamAges(teamAgesChicks);
        System.out.println("\nКоманда быків:");
        printTeamAges(teamAgesBulls);

        float averageAgeChicks = calculateAverageAge(teamAgesChicks);
        float averageAgeBulls = calculateAverageAge(teamAgesBulls);
        System.out.println("\nСередній вік команди чайок: " + averageAgeChicks);
        System.out.println("Середній вік команди биків: " + averageAgeBulls);
    }

    public static int[] generateTeamAges() {
        Random random = new Random();
        int[] teamAges = new int[25];
        for (int i = 0; i < teamAges.length; i++) {
            teamAges[i] = random.nextInt(23) + 18;
        }
        return teamAges;
    }

    public static void printTeamAges(int[] teamAges) {
        for (int i = 0; i < teamAges.length; i++) {
            System.out.print(teamAges[i] + " ");
            if ((i + 1) % 25 == 0) {
                System.out.println();
            }
        }
    }

    public static float calculateAverageAge(int[] teamAges) {
        float sum = 0;
        for (int age : teamAges) {
            sum += age;
        }
        return sum / teamAges.length;
    }
}